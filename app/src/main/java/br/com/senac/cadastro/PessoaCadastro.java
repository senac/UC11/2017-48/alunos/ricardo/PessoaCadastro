package br.com.senac.cadastro;

/**
 * Created by sala302b on 01/02/2018.
 */

public class PessoaCadastro {

    private String nomeCompleto;
    private String cidade;
    private String UF;
    private String profissao;
    private String nomeEmpresa;
    private String telefone;
    private String email;
    private String observacao;










    public PessoaCadastro(){

    }








    public PessoaCadastro(String nomeCompleto, String cidade, String UF, String profissao, String nomeEmpresa, String telefone, String email, String observacao){
        this.nomeCompleto = nomeCompleto;
        this.cidade = cidade;
        this.UF = UF;
        this.profissao = profissao;
        this.nomeEmpresa = nomeEmpresa;
        this.telefone = telefone;
        this.email = email;
        this.observacao = observacao;
    }





    public String getCidade() {
        return cidade;

    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }



}



